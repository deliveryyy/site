import Panel from '@/pages/Panel'
import App from '@/pages/App'
import List from '@/pages/restaurants/List'
import Details from '@/pages/restaurants/Details'
import Payment from '@/pages/Payment'

export default [
  {
    path: '/app',
    name: 'Panel',
    redirect: '/app/home',
    component: Panel,
    children: [
      {
        path: '/app/home',
        name: 'App',
        component: App
      },
      {
        path: '/app/list',
        name: 'List',
        component: List
      },
      {
        path: '/app/restaurant/:id',
        name: 'Details',
        component: Details
      },
      {
        path: '/app/payment',
        name: 'Payment',
        component: Payment
      },
    ]
  }
]