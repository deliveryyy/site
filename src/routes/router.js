import Vue from 'vue';
import Router from 'vue-router';
import AuthPermission from './../helpers/AuthPermission'

Vue.use(Router);

import Login from '@/pages/Login'
import Admin from '@/pages/AdminPanel'
import App from './app/index'

const router = new Router({
  mode: 'history',
  routes: [
    ...App,
    {
      path: '/login',
      component: Login,
      name: 'Login'
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/admin',
      component: Admin,
      name: 'Admin',
      meta: { p: 2 },
      beforeEnter: AuthPermission
    },
  ]
})


const redirectToLogin = (to) => {
  let token = Vue.$cookies.get('app_delivery_auth')
  let empty = (!token) || (token == undefined) || (token == '')
  
  return (empty) && (to != '/login')
}

router.beforeEach((to, from, next) => {
  if (redirectToLogin(to.path)) {
    next('/login');
  } else {
    next();
  }
})

// handler permissions
export default router
