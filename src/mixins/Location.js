export default {
  data() {
    return {
      $position: null
    }
  },
  methods: {
    getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.showPosition)
      }
      else {
        throw new Error('Navegador não suporta')
      }
    },
    showPosition(position) {
      console.log('deu certo', position)
      localStorage.delivery_geo = position
    }
  },
  created() {
    if (localStorage.delivery_geo)
      this.$position = localStorage.delivery_geo.coords
  }
}