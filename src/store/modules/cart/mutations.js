export default { 
  ADD_ITEM (state, val) {
    state.cart.push(val)
  },
  DEL_ITEM (state, index) {
    state.cart = state.cart.filter((e, i) => i != index)
  }
}