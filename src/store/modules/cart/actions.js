export default { 
  async addItenCart ({ commit }, item) {
    await commit('ADD_ITEM', item) 
  },
   async deleteItenCart({ commit }, index) {
     await commit('DEL_ITEM', index)
   }
}