export default { 
  cart(state) {
    return state.cart
  },
  cartValueTotal (state) {
    let total = 0 
    state.cart.map(e => {
      total += e.valor
      return e
    })

    return total
  },
  countCart (state) {
    return state.cart.length
  }
}