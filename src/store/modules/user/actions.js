export default { 
  async setUser({ commit }, user) {
    await commit('SET_USER', user)
  }
}