export default { 
  async setListCompanies({ commit }, list) {  
    await commit('SET_LIST', list)
  }
}