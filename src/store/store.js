import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import axios from "axios";
import { config } from "@/modules/config";

// import modules
import User from '@/store/modules/user/index'
import Cart from '@/store/modules/cart/index'
import Company from '@/store/modules/company/index'

Vue.use(Vuex)

const vuexLS = new VuexPersist({
  key: 'nordsee',
  storage: window.localStorage,
  modules: ['User', 'Cart'],
  reducer: state => ({
    http: state.http
  })
})

const store = new Vuex.Store({
  plugins: [vuexLS.plugin],
  modules: {
    User,
    Cart,
    Company
  },
  state: {
    http: null
  },
  mutations: {
    setAxios: (state, http) => state.http = http
  },
  actions: {
    createAxios: ({ commit }) => {
      try {
        console.log(config.api)
        let http = axios.create({
          baseURL: config.api
        })
  
        //http.defaults.headers["v-token"] = 'token de acesso do user'
        http.defaults.headers["Access-Control-Allow-Origin"] = '*'
        commit('setAxios', http)
      } catch (e) {
        console.log(e)
      }
    }
  },
  getters: {
    $http: state => state.http
  }
})

export default store