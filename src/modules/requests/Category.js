import { mapGetters } from "vuex";

export default {
  computed: {
    ...mapGetters(['$http'])
  },
  data() {
    return {
      Categories: {
        list: []
      }
    }
  },
  methods: {
    async listCategories () {
      try {
        
        let aux = (await this.$http.get('/Categoria/ListarCategorias')).data
        this.Categories.list = aux

      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro Interno', 'error')
      }
    },

    async storeCategory (category) {
      try {

        await this.$http.post('/Categoria/Adicionar', {
          id: 0,
          nome: category.nome
        })

        this.$swal.fire('Categoria', 'Item criando', 'succes')

      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro Interno', 'error')
      }
    }
  }
}