import { mapGetters, mapActions } from "vuex";

export default {
  computed: {
    ...mapGetters(['$http'])
  },
  methods: {
    async storeOrder(order) {
      try {

        await this.$http.post('/Pedido/Adicionar', {
          pessoaId: order.pessoaId,
          empresaId: order.empresaId,
          produtoid: order.produtoId,
          valor: order.valor,
          statusId: order.statusId
        })

      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro ao salvar pedido', 'error')
      }
    }
  }
}