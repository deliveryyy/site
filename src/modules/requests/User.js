import { mapGetters, mapActions } from "vuex";

export default {
  computed: {
    ...mapGetters(['$http'])
  },
  methods: {
    ...mapActions('User', ['setUser']),
    async auth ({ email, password }) {
      try {
        let res = await this.$http.post('/Login/Acessar', {
          email: email,
          senha: password
        })

        if (res.status == 200) {

          this.$cookies.set('app_delivery_auth', true)
          

          let aux = res.data
          aux.endereco = await this.showAddress(aux.id)
          await this.setUser(aux)
          localStorage.setItem('app_user', JSON.stringify(aux))

          if (res.data.nivelAcesso == 2) {
            this.$router.push('/admin')
          }
          else {
            this.$router.push('/app')
          }
          
        }
        else {
          this.$swal.fire('Login', 'Dados incorreto, tente novamente', 'error')
        }

      } catch (e) {
        console.log(e)
        this.$swal.fire('Login', 'Erro ao fazer autenticação, tente novamente', 'error')
      }
    },

    async me ({ email, password }) {
      try {
        let res = await this.$http.post('/Login/Acessar', {
          email: email,
          senha: password
        })

        if (res.status == 200) {
          let aux = res.data
          await this.setUser(aux)
        }
        return res.data

      } catch (e) {
        console.log(e)
        this.$swal.fire('Login', 'Erro ao fazer autenticação, tente novamente', 'error')
      }
    },

    async register (form, endereco) {
      try {
        await this.$http.post('/Login/Cadastrar', {
          "cpf": form.cpf,
          "nome": form.nome,
          "dataNascimento": form.dataNascimento,
          "email": form.email,
          "senha": form.senha
        })

        await this.$swal.fire({
          title: 'Cadastro',
          text: "cadastro realizado com sucesso!",
          icon: 'success',
          showCancelButton: false,
          confirmButtonText: 'Continuar'
        }).then(async (result) => {
          if (result.value) {
            
            let data = await this.me({
              email: form.email,
              password: form.senha
            })  

            await this.storeAddress(endereco, data)

          }
        })
        
      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro no cadastro', 'error')
      }
    },
    
    async storeAddress (address, user) {
      try {

        await this.$http.post('/Endereco/Adicionar', {
          "id": 0,
          "pessoaId": user.id,
          "logradouro": address.logradouro,
          "numero": address.numero,
          "complemento": address.complemento,
          "estado": address.estado,
          "cidade": address.cidade
        })

        if (user.nivelAcesso == 2) {
          this.$router.push('/admin')
        }
        else {
          this.$router.push('/app')
        }

      } catch (e) {
        console.log(e)
      }
    },


    async showAddress (userId) {
      try {
        return (await this.$http.post('/Endereco/ListarEnderecoPorId', {
          pessoaId: userId
        })).data
      } catch(e) {
        console.log(e)
      }
    },

    logout() {
      this.$cookies.remove('app_delivery_auth')
      localStorage.removeItem('app_user')
      this.$router.push('/login')
    }
  }
}