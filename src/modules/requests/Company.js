import { mapGetters, mapActions } from "vuex";

export default {
  computed: {
    ...mapGetters(['$http'])
  },
  data() {
    return {
      Companies: {
        list: [],
        filter: [],
        data: {
          id: 0,
          razaosocial: '',
          cnpj: '',
          nomefantasia: '',
          descricao: '',
          valorentrega: 0
        }
      },
      loadListCompanies: false,
    }
  },
  methods: {
    ...mapActions('Company', ['setListCompanies']),
    async listCompanies () {
      try {
        this.loadListCompanies = true
        this.Companies.list = (await this.$http.get('/Empresa/ListarEmpresas')).data    
        await this.setListCompanies(this.Companies.list)
        this.loadListCompanies = false
      } catch (e) {
        this.loadListCompanies = false
        console.log(e)
        this.$swal.fire('Erro', 'Erro', 'error')
      }
    },

    async getCompany ({ id }) {
      try {
        this.Companies.data = (await this.$http.get('/Empresa/Detalhe/' + id)).data
      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro', 'error')
      }
    },

    async storeCompany (company) {
      try {
        
        await this.$http.post('/Empresa/Adicionar', {
          "id": 0,
          "razaosocial": company.razaosocial,
          "cnpj": company.cnpj,
          "nomefantasia": company.nomefantasia,
          "valorentrega": company.valorentrega
        })
        this.$swal.fire('Sucesso', 'Item criado', 'success')

      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro', 'error')
      }
    } 
  }
}