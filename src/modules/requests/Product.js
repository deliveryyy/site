import { mapGetters } from "vuex";

export default {
  computed: {
    ...mapGetters(['$http'])
  },
  data() {
    return {
      Products: {
        data: {},
        list: []
      },
      loadListProducts: false,
    }
  },
  methods: {
    async listProducts ({ empresa }) {
      try {
        
        this.loadListProducts = true
        this.Products.list = (await this.$http.get('/Produto/ListarPorEmpresa?empresaId=' + empresa)).data
        this.loadListProducts = false

      } catch (e) {
        this.loadListProducts = false
        console.log(e)
        this.$swal.fire('Erro', 'Erro', 'error')
      }
    },

    async storeProdutcts (product) {
      try {
        
        await this.$http.post('/Produto/Adicionar', {
          "id": product.id,
          "nome": product.nome,
          "descricao": product.descricao,
          "valor": product.valor,
          "valido": 1,
          "empresaId": product.empresaId,
          "categoriaId": product.categoriaId,
          "valorPromocional": product.valor
      })
      
      this.$swal.fire('Sucesso', 'Item criado', 'success')

      } catch (e) {
        console.log(e)
        this.$swal.fire('Erro', 'Erro', 'error')
      }
    }

  }
}