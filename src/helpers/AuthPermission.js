import store from '@/store/store'

function AuthPermission (to, from, next) {
  if ('meta' in to && 'p' in to.meta) {
    let nivelAcesso = store.state.User.user.nivelAcesso || (JSON.parse(localStorage.getItem('app_user'))).nivelAcesso 
    if (to.meta.p == nivelAcesso) {
      return next()
    }
    else {
      alert('Sem permissão')
      return next('/app/home')
    }
  }
  else {
    return next()
  }
}

export default AuthPermission