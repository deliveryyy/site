import Vue from 'vue'
import Main from './Main.vue'
import vuetify from './plugins/vuetify'
import router from './routes/router'
import store from './store/store'
import VueSweetalert2 from 'vue-sweetalert2';
 
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2);

import './plugins/VueCookie'
import './plugins/mask'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(Main)
}).$mount('#app')
